import App from './App.svelte';



const app = new App({
    target: document.body,
    props: {
        link: document.querySelector('[data-link_lgpd]').dataset.link_lgpd,
        texto: document.querySelector('[data-link_lgpd]').dataset.texto_lgpd
    }
});

export default app;
